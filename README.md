# Carnext Microservice on AWS

## Goal
The goal of this project is designing and implementing a simple microservice architecture on AWS using python. 

## Introduction
In this project, I am supposed to implement two services.

- **Inventory application**
- **Predict Price application**

The former is responsible for providing a public Rest API, communicate with RDS, and also get the result from Price app. 
The latter, using a model, will predict the price of a car.
This project consists of three parts:

- **Carnext Infra**: Using AWS CDK, Creates required Microservice infra on AWS
- **Inventory App**: A restful service containing Inventory logic
- **Pricing App**: A simple service containing pricing logic
  
## Technologies and Libraries in services
In services, I used:

- [FastApi](https://fastapi.tiangolo.com/) for handling rest services
- [SqlAlchemy](https://www.sqlalchemy.org/) as the ORM
- [Configparser](https://docs.python.org/3/library/configparser.html) to read config files
- [Requests](https://requests.readthedocs.io/en/master/) to handle communication betIen services
- [Logzero](https://logzero.readthedocs.io/en/latest/) for logging purposes

## Technologies for Infra
We intensively used CDK for this project. The infra consists of four stacks:

- VPC
- RDS (Mysql)
- ECS
- ALB

## Non Functional requirements
#### Availability
As I deployed our services using CDK, inside the ECS stack, I can set up 2 or more tasks/services. So the service is always available.
I also don't have any problem when I want to update our services.

#### Security
Is handle by IAM roles, Security groups, etc

#### Service Discovery
By means of an ALB, when I change a service or task, Both ALB and ECS handle this change to us, and service is always available.

## Improvements
### Separate Repositories
One of intrinsic characteristics of a microservice is the ability to be deployed separately and solly!
At the moment, some simple build and deploy mechanism is implement for each subproject, and I did my best to implement 
them separately for the sake of future splits.
But for sure, at the time of implementing CI/CD, I will need them to be separated. 

### Send Container logs to ELK stack
At the moment, I send our logs to CloudWatch. But when the number of containers grows, ELK stack also provides good search possibilities using Kibana.

### Use a message broker
Kafka is the 'defacto' for such technologies. As I am going to ingest data from multiple sources, Using Kafka will help
a lot. It can be used both for the input of our system, or the output.

- We can subscribe a number of producers or consumers for each topic
- Data retention is another factor 
- etc, in case of the need

### Use an API Gateway
It will help us to gather our functional or non-functional requirements that should be available for all/a part of our services


### Monitoring
AWS provides a bunch of facilities to monitor applications in the cloud. Besides, it is also 
a good practice to have custom metrics to monitor our system in application layer. 