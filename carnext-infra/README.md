# Carnext Infra!

This is Carnext Infra project for development with CDK.
It contains four stacks:
* vpc stack: responsible for creating vpc
* rds stack: responsible for creating a Mysql database instance
* ecs stack: responsible for creating ECS cluster, tasks, and services
* alb stack: responsible for routing internet traffic to the project

## How to run
- Take care of [prerequisites](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html#getting_started_prerequisites) for installing CDK
- [Install CDK](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html#getting_started_install)
- In command line, run "npm install"
- Then run one of useful links below

## Useful commands

 * `npm run build`   compile typescript to js
 * `npm run watch`   watch for changes and compile
 * `npm run test`    perform the jest unit tests
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template

You can use "npx cdk deploy --all --require-approval=never" to deploy all stacks without prompting (yes/no)
