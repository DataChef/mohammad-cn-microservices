import cdk = require('@aws-cdk/core');
import {VpcStack} from "../lib/vpc-stack";
import {RdsStack} from "../lib/rds-stack";
import {EcsStack} from "../lib/ecs-stack";
import {AlbStack} from "../lib/alb-stack";

const app = new cdk.App();

require('dotenv').config();
const cdkEnvironment = process.env.CDK_ENVIRONMENT!;
const context = app.node.tryGetContext(cdkEnvironment);

const vpcStack = new VpcStack(app, 'carnext-vpc-stack', context);
const rdsStack = new RdsStack(app, 'carnext-rds-stack', {vpc: vpcStack.vpc}, context);
const ecsStack = new EcsStack(app, 'carnext-ecs-stack', {vpc: vpcStack.vpc, db: rdsStack.database}, context);
const albStack = new AlbStack(app, 'carnext-alb-stack', {vpc: vpcStack.vpc, service: ecsStack.service, hostPort: ecsStack.hostPort}, context);

app.synth();

