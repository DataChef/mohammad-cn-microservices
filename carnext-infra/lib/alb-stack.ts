import * as cdk from '@aws-cdk/core';
import ec2 = require('@aws-cdk/aws-ec2');
import elbv2 = require('@aws-cdk/aws-elasticloadbalancingv2');
import ecs = require('@aws-cdk/aws-ecs');
import {Construct} from "@aws-cdk/core";

export interface AlbStackProps extends cdk.StackProps {
  vpc: ec2.IVpc
  service: ecs.Ec2Service
  hostPort: number
}

export class AlbStack extends cdk.Stack {

  constructor(scope: Construct, id: string, props: AlbStackProps, context: any) {
    super(scope, id, props);

    // Create ALB
    const lb = new elbv2.ApplicationLoadBalancer(this, 'carnext-alb', {
      vpc: props.vpc,
      internetFacing: true
    });
    const listener = lb.addListener('carnext-public-listener', { port: context['alb-listener-port'], open: true });

    // Attach ALB to ECS Service
    listener.addTargets('ECS', {
      port: context['alb-target-port'],
      targets: [props.service.loadBalancerTarget({
        containerName: 'inventory-app-container',
        containerPort: props.hostPort
      })],
    });

    new cdk.CfnOutput(this, 'carnext-load-balancer-DNS', { value: lb.loadBalancerDnsName });
  }
}
