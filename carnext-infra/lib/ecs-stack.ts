import * as cdk from '@aws-cdk/core';
import ec2 = require('@aws-cdk/aws-ec2');
import ecs = require('@aws-cdk/aws-ecs');
import ecr = require('@aws-cdk/aws-ecr')
import rds = require('@aws-cdk/aws-rds')
import {Construct} from "@aws-cdk/core";

export interface EcsStackProps extends cdk.StackProps {
  vpc: ec2.IVpc
  db: rds.DatabaseInstance
}

export class EcsStack extends cdk.Stack {
  public readonly service: ecs.Ec2Service
  public readonly hostPort: number

  constructor(scope: Construct, id: string, props: EcsStackProps, context: any) {
    super(scope, id, props);

    // Cluster
    const cluster = new ecs.Cluster(this, 'carnext-ecs-cluster', { vpc: props.vpc });
    cluster.addCapacity('DefaultAutoScalingGroup', {
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.MICRO)
    });

    // ECR repository
    const inventoryRepo = ecr.Repository.fromRepositoryName(this, 'inventory-app', 'inventory-app' )

    // Create Task Definition
    const taskDefinition = new ecs.Ec2TaskDefinition(this, 'TaskDef');
    const logging = new ecs.AwsLogDriver({ streamPrefix: "inv-app" })

    // Create Container
    const container = taskDefinition.addContainer('inventory-app-container', {
      image: ecs.ContainerImage.fromEcrRepository(inventoryRepo),
      memoryLimitMiB: context['ecs-container-memoryLimitMiB'],
      logging,
      environment: {
        DB_ENDPOINT: props.db.dbInstanceEndpointAddress,
        DB_USER: context['db-user'],
        DB_NAME: context['db-name'],
        DB_SECRET_NAME: props.db.secret?.secretName || "secret-name",
      },
    });

    container.addPortMappings({
      containerPort: 80,
      hostPort: 0,
      protocol: ecs.Protocol.TCP
    });

    // Create Service
    this.service = new ecs.Ec2Service(this, "inventory-app-service", {
      cluster,
      taskDefinition,
    });

    this.hostPort = container.portMappings[0].hostPort || 0
  }
}
