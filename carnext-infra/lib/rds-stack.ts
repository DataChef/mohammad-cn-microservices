import * as cdk from '@aws-cdk/core';
import ec2 = require('@aws-cdk/aws-ec2');
import rds = require('@aws-cdk/aws-rds')
import {Construct} from "@aws-cdk/core";

export interface RdsStackProps extends cdk.StackProps {
  vpc: ec2.IVpc
}

export class RdsStack extends cdk.Stack {
  public readonly database: rds.DatabaseInstance

  constructor(scope: Construct, id: string, props: RdsStackProps, context: any) {
    super(scope, id, props);

    this.database = new rds.DatabaseInstance(this, 'MysqlInstance', {
        databaseName: context['db-name'],
      engine: rds.DatabaseInstanceEngine.mysql({
        version: rds.MysqlEngineVersion.VER_8_0_20,
      }),
      vpc: props.vpc,
      port: context['db-port'],
      vpcSubnets: {
        subnetType: ec2.SubnetType.PUBLIC,
      },
      instanceType: ec2.InstanceType.of(ec2.InstanceClass.T2, ec2.InstanceSize.SMALL),
      publiclyAccessible: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    })

    this.database.connections.allowFrom(ec2.Peer.ipv4(context['rds-cidrip']), ec2.Port.tcp(context['db-port']))
  }
}
