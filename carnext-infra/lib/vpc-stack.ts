import * as cdk from '@aws-cdk/core';
import ec2 = require('@aws-cdk/aws-ec2');

export class VpcStack extends cdk.Stack {
  public readonly vpc: ec2.IVpc

  constructor(scope: cdk.Construct, id: string, context: any, props?: cdk.StackProps) {
    super(scope, id, props);

    this.vpc = new ec2.Vpc(this, 'carnext-vpc', { maxAzs: context['vpc-maxAzs'],});
  }
}
