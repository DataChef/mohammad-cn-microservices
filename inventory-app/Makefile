.DEFAULT_GOAL:=help
.RUN:=poetry run

.PHONY: build
build: ## Build package
	poetry build

.PHONY: rsetup
rsetup: ## Removes poetry.lock file and Creates a virtual environment and installs required dependencies from scratch
	rm poetry.lock
	poetry install --no-root -vvv

.PHONY: setup
setup: ## Creates a virtual environment and installs required dependencies using 'poetry.lock' file
	poetry install --no-root -vvv

.PHONY: local
local: ## Runs the project on local docker
	#docker rm $(docker ps -aq --filter name=inv-app-container)
	docker build -t inventory-app .
	docker run  --env-file .env --name inv-app-container -p 80:80 inventory-app

.PHONY: create-aws-repo
create-aws-repo: ## Creates inventory-app repository inside AWS ECR
	chmod +x scripts/create-aws-repo.sh
	sh scripts/create-aws-repo.sh

.PHONY: deploy
deploy: ## Creates a new docker image out of existing source code and deploys inventory-app docker image into AWS ECR
	chmod +x scripts/deploy.sh
	sh scripts/deploy.sh

.PHONY: init_db
init_db: ## initialize database with seed data
	python app/inserts.py

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*?## "; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

