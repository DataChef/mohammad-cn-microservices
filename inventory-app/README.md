# Inventory App

A python app which is supposed to be deployed on Docker, EC2, ECS

This app is responsible for:
- Provide a Rest API 
- Get inventory information from RDS
- Get information of price value from another service in ECS

## Send request to this app
This app answers to the post requests with path /price and a json as input with values make, model, and market_val.  
A sample request can be like:
```shell
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"make":"BMW","model":"S3","market_val":13580}' \
  http://localhost/price
```