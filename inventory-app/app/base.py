from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from app.config import settings

engine = create_engine('mysql+pymysql://{}:{}@{}/{}'.format(
    settings.database.username,
    settings.dbsec.password,
    settings.database.endpoint,
    settings.database.db_name
))
Session = sessionmaker(bind=engine)

Base = declarative_base()
