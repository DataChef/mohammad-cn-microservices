from sqlalchemy import Column, String, Integer
from app.base import Base


class Car(Base):
    __tablename__ = 'car'

    id = Column(Integer, primary_key=True, autoincrement=True)
    make = Column(String(32))
    model = Column(String(32))
    count = Column(Integer)

    def __init__(self, make, model, count):
        self.make = make
        self.model = model
        self.count = count

    def __repr__(self):
        return (
            "<Car('{self.make}', '{self.model}', '{self.count}')>".format(self=self)
        )

    def __eq__(self, other):
        return isinstance(other, Car) and other.id == self.id
