from fastapi import FastAPI, HTTPException
from app.base import Session
from app.car import Car
from pydantic import BaseModel
import requests
from logzero import logger
from app.config import settings
import json

app = FastAPI()


class InputRequest(BaseModel):
    make: str
    model: str
    market_val: int


@app.post("/price/")
def get_price(req: InputRequest):
    logger.info("Hi price!, {}, {}, {}".format(req.make, req.model, req.market_val))

    if len(req.make) > 1 and len(req.model) > 1 and req.market_val > 1:
        car = get_car(req.make, req.model, Session())
        if car.count > 0:
            return get_predicted_price(req.market_val, car.count)
        else:
            raise HTTPException(status_code=404, detail="Car not found.")
    else:
        raise HTTPException(status_code=400, detail="Bad Request.")


def get_car(make, model, session):
    try:
        car = session.query(Car) \
            .filter(Car.make == make) \
            .filter(Car.model == model) \
            .first()

        return car

    except Exception as e:
        logger.error('Failed to get car with make={}, model={}'.format(make, model))
        logger.exception(e)
        raise HTTPException(status_code=404, detail="Car not found.")


def get_predicted_price(market_val, count):
    try:
        response = requests.post(settings.price.endpoint,
                                 json.dumps({'market_val': market_val, 'count': count}),
                                 headers={'content-type': 'application/json'}
                                 )
        if response.status_code == 200:
            return response
        else:
            raise HTTPException(status_code=404, detail=response.json())

    except Exception as e:
        logger.error('Failed to get price from our price service! market_val={}, count={}'
                     .format(market_val, count))
        logger.exception(e)
        raise HTTPException(status_code=404, detail="Predicted price is not available.")
