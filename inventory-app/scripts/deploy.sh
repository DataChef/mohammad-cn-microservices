#!/bin/sh

set -e

docker build -t inventory-app .
docker tag inventory-app ${ECR_INVENTORY_PATH}
aws ecr get-login-password | docker login --username AWS --password-stdin ${ECR_REPOSITORY_PATH}
docker push ${ECR_INVENTORY_PATH}

aws ecs update-service --cluster ${ECS_CLUSTER_NAME} --service ${ECS_INVENTORY_SERVICE_NAME} --force-new-deployment

echo "Docker Image successfully pushed to 'inventory-app' repository."