import unittest

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app.base import Base
from app.car import Car


class TestCar(unittest.TestCase):

    engine = create_engine('sqlite:///:memory:')
    Session = sessionmaker(bind=engine)
    session = Session()

    def setUp(self):
        Base.metadata.create_all(self.engine)
        self.car = Car(make='Ford', model='Mustang', count=5)
        self.session.add(self.car)
        self.session.commit()

    def tearDown(self):
        Base.metadata.drop_all(self.engine)

    def test_query_panel(self):
        expected = [self.car]
        result = self.session.query(Car).all()
        self.assertEqual(result, expected)
