import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.base import Base
from app.car import Car
from app.main import get_car, get_predicted_price
import responses
import json
from app.config import settings


class TestMain(unittest.TestCase):

    engine = create_engine('sqlite:///:memory:?check_same_thread=False')
    Session = sessionmaker(bind=engine)
    session = Session()

    def setUp(self):
        Base.metadata.create_all(self.engine)

        self.toyota_supra = Car(make='Toyota', model='Supra', count=5)
        self.session.add(self.toyota_supra)

        self.ford_mustang = Car(make='Ford', model='Mustang', count=3)
        self.session.add(self.ford_mustang)

        self.session.commit()

    def tearDown(self):
        Base.metadata.drop_all(self.engine)

    def test_get_toyota(self):
        expected = self.toyota_supra
        result = get_car('Toyota', 'Supra', self.session)
        assert result.id == 1
        self.assertEqual(result, expected)

    def test_get_ford(self):
        expected = self.ford_mustang
        result = get_car('Ford', 'Mustang', self.session)
        assert result.id == 2
        self.assertEqual(result, expected)

    @responses.activate
    def test_get_p_price(self):
        def request_callback(request):
            payload = json.loads(request.body)
            resp_body = {'result': 1955.923}
            headers = {'request-id': '728d329e-0e86-11e4-a748-0c84dc037c13'}
            return 200, headers, json.dumps(resp_body)

        responses.add_callback(
            responses.POST, settings.price.endpoint,
            callback=request_callback,
            content_type='application/json',
        )

        result = get_predicted_price(1, 2)
        assert result.json() == {'result': 1955.923}

