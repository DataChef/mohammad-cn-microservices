# Pricing App

A python app which is supposed to be deployed on Docker, EC2, ECS

This app is responsible for predicting estimated sell price:
- Provide a Rest API on a path "/price" with parameters "market_value" and "count"
- Get price information from a PKL model


