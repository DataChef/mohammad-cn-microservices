import os
from os import path
import pickle
from fastapi import FastAPI, HTTPException, Path
from pydantic import BaseModel

app = FastAPI()


class PriceRequest(BaseModel):
    market_val: int = Path(..., title="The market value of the car", gt=0)
    count: int = Path(..., title="The number of similar existing cars in stock", gt=0)


@app.post("/price/")
def get_predicted_price(req: PriceRequest):
    # if req.count < 1 or req.market_val < 1:
    #     raise HTTPException(status_code=400, detail="Input request is not valid.")

    x_test = [(req.market_val, req.count)]
    pkl_path = path.join(os.path.dirname(__file__), 'model.pkl')

    with open(pkl_path, "rb") as pkl:
        model = pickle.load(pkl)
        result = model.predict(x_test)
        return {"result": round(result[0], 3)}



