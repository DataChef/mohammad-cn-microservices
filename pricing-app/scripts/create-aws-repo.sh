#!/bin/sh

set -e

aws ecr create-repository --repository-name ${ECR_REPOSITORY_NAME} --region eu-west-1