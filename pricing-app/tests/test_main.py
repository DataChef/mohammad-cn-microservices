import unittest
from fastapi.testclient import TestClient
from app.main import app

client = TestClient(app)


class TestMain(unittest.TestCase):

    def test_price_bad_market_value(self):
        response = client.post(
            "/price/",
            json={"market_val": 0, "count": 23},
        )

        assert response.status_code == 422

    def test_price_bad_count(self):
        response = client.post(
            "/price/",
            json={"market_val": 13689, "count": 0},
        )
        assert response.status_code == 422

    def test_price_bad_parameters(self):
        response = client.post(
            "/price/",
            json={"market_val": 0, "count": 0},
        )
        assert response.status_code == 422

    def test_price_result(self):
        response = client.post(
            "/price/",
            json={"market_val": 13689, "count": 23},
        )
        assert response.status_code == 200
        assert response.json()['result'] == 1955.923
